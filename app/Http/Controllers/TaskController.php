<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task as Task;

class TaskController extends Controller
{
    public function index()
    {
        $data = Task::where('archive', 0)->orderBy('id', 'asc')->get();
        $data = json_encode($data);
        return $data;
    }

    public function create($task)
    {
        if(!$task)
            Task::create(['body' => $task]);
        $data = Task::where('archive', 0)->orderBy('id', 'asc')->get();
        $data = json_encode($data);
        return $data; 
    }

    public function completed(){
        $data = Task::where('archive', 1)->orderBy('id', 'asc')->get();
        $data = json_encode($data);
        return $data;
    }

    public function complete($id)
    {
        Task::where('id',$id)->update(['archive' => 1]);
        $data = Task::where('archive', 0)->orderBy('id', 'asc')->get();
        $data = json_encode($data);
        return $data; 
    }

    public function delete($id)
    {
        Task::where('id',$id)->delete();
        $data = Task::where('archive', 0)->orderBy('id', 'asc')->get();
        $data = json_encode($data);
        return $data; 
    }
}
