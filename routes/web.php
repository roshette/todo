<?php


Route::get('/', function () {
    return view('main');
});

//Route::get('/', 'TaskController@index');
Route::get('current_tasks/', 'TaskController@index');
Route::get('create_task/{task}', 'TaskController@create');
Route::get('complete_task/{id}', 'TaskController@complete');
Route::get('completed_tasks/', 'TaskController@completed');
Route::post('delete_task/{id}', 'TaskController@delete');
