import axios from 'axios';

require('./bootstrap');

window.Vue = require('vue');
window.axios = axios;

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
	el: '#app',
	data: {
		todos: [],
		task: ''
	},
	methods: {
		addTask: function () {
			axios.get('create_task/' + this.task).then(response => this.todos = response.data);
			this.task = '';
		},
		completeTask(id) {
			axios.get('complete_task/' + id).then(response => this.todos = response.data);
		},
		completedTasks: function () {
			axios.get('completed_tasks/').then(response => this.todos = response.data);
		},
		deleteTask(id) {
			axios.post('delete_task/' + id).then(response => this.todos = response.data);
		},
		notCompletedTasks: function () {
			axios.get('current_tasks/').then(response => this.todos = response.data);
		}
	},
	mounted() {
		axios.get('current_tasks/').then(response => this.todos = response.data);
	}
	// Ya zaebalsa
});
