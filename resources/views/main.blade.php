<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Todo</title>
    <link rel="stylesheet" type="text/css" href="css/app.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.Laravel = {csrfToken: '{{ csrf_token() }}'}
    </script>
</head>
<body>

<div id='app'>
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="newTask">
                    <ol>
                        <li v-for="todo in todos">
                            @{{ todo.body }}
                            <a v-if="todo.archive = 0" class="complete-link" v-on:click.prevent="completeTask(todo.id)">Complete</a>
                            <a class="delete-link" v-on:click.prevent="deleteTask(todo.id)">Delete</a>
                        </li>
                    </ol>
                </div>
                <div class="newTask">
                    <input v-model="task" type="text" class="field-task" placeholder="newTask"><br>
                    <input v-on:click="addTask" class="btn add-task" type="submit" value="Create awesome!">
                </div>
                <div class="completedTask">
                    <input v-on:click="completedTasks" class="btn add-task" type="submit" value="Completed tasks!">
                </div>
                <div class="notCompletedTask">
                    <input v-on:click="notCompletedTasks" class="btn add-task" type="submit"
                           value="Not Completed tasks!">
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>

<script src="js/app.js"></script>
</body>
</html>